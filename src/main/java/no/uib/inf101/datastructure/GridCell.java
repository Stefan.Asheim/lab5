package no.uib.inf101.datastructure;

// Les om records her: https://inf101.ii.uib.no/notat/mutabilitet/#record

import java.awt.Color;

/**
 * A CellColor contains a CellPosition and a Color.
 *
 * @param cellPosition  the position of the cell
 * @param elem        the value of the cell
 */
public record GridCell <T>(CellPosition pos, T elem) { }
